/**
 * @ClassName: Demo
 * @Description: 测试hello,world
 * @author xukaiqiang
 * @date 2016年5月2日 下午4:53:51
 * @modifier
 * @modify-date 2016年5月2日 下午4:53:51
 * @version 1.0
 */
public class Demo {
	public static void main(String[] args) {
		System.out.println("hello,world!");
		System.out.println("我要测试再次提交的操作。");
	}
}
